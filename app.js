p5.disableFriendlyErrors = true;

const FLOOR_POS = 360;

const DEFAULT_SPEED = 6;
const VELOCITY_SPEED = 1.0002; // Vitesse du mouton augmente avec le temps
const BOOST_SPEED = 30;
const BOOST_DURATION = 5000; // 5000 = 5s
const BOOST_DURATION_INVINCIBILITY = 6000; // 6000 = 6s

const DIR_ASSETS = "assets/";


var sheep, sprFloor;
var sheepJumping = false;
var bg;

var decorFloor = [];
var imgCloud = [];
var kebabs = [];

var start = false;
var backgroundUsed;

var obstacles = [];
var obstaclesType = []
var obstaclesPassed = 0;
var moise_character;

var endGameDone = false;

var nickname = '';

getScore();

function setup() {
  createCanvas(1000, 400);
  camera.on();

  sheep = new Sheep();

  obstaclesType = [
    {
      'name': 'fence',
      'image': loadImage(DIR_ASSETS+"images/obstacles/fence.png"),
      'scale': 1,
      'y': 340,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    }, {
      'name': 'mower',
      'image': loadImage(DIR_ASSETS+"images/obstacles/mower.png"),
      'scale': 1,
      'y': 350,
      'collider': {
        'offsetX': 0,
        'offsetY': 0,
        'width': 90,
        'height': 45
      }
    }, {
      'name': 'lightning',
      'image': loadImage(DIR_ASSETS+"images/obstacles/lightning.png"),
      'scale': 1,
      'y': 40,
      'collider': {
        'offsetX': 0,
        'offsetY': 0,
        'width': 25,
        'height': 150
      }
    },{
      'name': 'cactus',
      'image': loadImage(DIR_ASSETS+"images/obstacles/cactus.png"),
      'scale': 1,
      'y': 340,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    },{
      'name': 'rocher',
      'image': loadImage(DIR_ASSETS+"images/obstacles/rocher.png"),
      'scale': 1,
      'y': 350,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    },{
      'name': 'present',
      'image': loadImage(DIR_ASSETS+"images/obstacles/present.png"),
      'scale': 1,
      'y': 350,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    },{
      'name': 'snowman',
      'image': loadImage(DIR_ASSETS+"images/obstacles/snowman.png"),
      'scale': 1,
      'y': 340,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    },{
      'name': 'poor',
      'image': loadImage(DIR_ASSETS+"images/obstacles/poor.png"),
      'scale': 1,
      'y': 340,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    },{
      'name': 'taxi',
      'image': loadImage(DIR_ASSETS+"images/obstacles/taxi.png"),
      'scale': 1,
      'y': 340,
      'collider': {
        'offsetX': 10,
        'offsetY': 0,
        'width': 40,
        'height': 100
      }
    }
  ];

  imgFlower = loadImage(DIR_ASSETS+"images/decor/flower/flower.png")
  imgKebab = loadImage(DIR_ASSETS+"images/boost/kebab_speed.png")
  imgCloud[1] = loadImage(DIR_ASSETS+"images/decor/cloud/cloud_01.png")
  imgCloud[2] = loadImage(DIR_ASSETS+"images/decor/cloud/cloud_02.png")

  backgroundUsed = parseInt(random(1, 5));
  bg = loadImage(DIR_ASSETS+"images/background_0" + backgroundUsed + ".png");
}

function draw() {
  background("#00BFFF");
  image(bg, camera.position.x - 500, 0);

  sheep.run();

  if(moise_character && moise_character.isPassed(sheep.getPosition().x)) {
    moise_character.sprMoise.remove();
    moise_character.sprResurection.remove();
    moise_character = null;
  }

  if (!start) {
    if(nickname == '') nickname = prompt("Please enter your nickname", "Harry Potter");
    textSize(40);
    text("Press SPACE to start the game!", sheep.getPosition().x - 300, 100);
  }

  // Ajout aléatoire des décors et nuages
  if (random(0, 50) < 1 && sheep.alive) {
    decorFloor.push(new DecorFloor(sheep.getPosition().x + 500, random(360, 390), 10, 20, imgFlower, 'flower'));
    decorFloor.push(new DecorFloor(sheep.getPosition().x + 800, random(20, 50), 10, 20, imgCloud[parseInt(random(1, 3))], 'cloud'));
  }
  // Ajout aléatoire des kebab's
  if (random(0, 3000) < 1 && sheep.alive && start) {
    kebabs.push(new Kebab(sheep.spr.position.x + 800, random(20, FLOOR_POS), imgKebab));
  }

  var tempDecorFloor = decorFloor;
  for (var i = 0; i < decorFloor.length; i++) {
    decorFloor[i].draw();
    if(decorFloor[i].canBeDeleted(sheep.spr.position.x)) {
      decorFloor[i].sprite().remove();
      tempDecorFloor.splice(i,1);
    }
  }
  decorFloor = tempDecorFloor;
  for (var i = 0; i < kebabs.length; i++) {
    kebabs[i].draw();
    if (sheep.spr.overlap(kebabs[i].sprite()) && !sheep.boosted) {
      sheep.spr.velocity.x = BOOST_SPEED;
      sheep.boosted = true;
      setTimeout(function(){
        sheep.spr.velocity.x = DEFAULT_SPEED;
      }, BOOST_DURATION);
      setTimeout(function(){
        sheep.boosted = false;
      }, BOOST_DURATION_INVINCIBILITY);
    }
  }

  // Ajout d'un obstacle toutes les 70 frames
  if (frameCount % 60 === 0 && start && sheep.alive) {
    var fenceAvailable = [];

    // de base il y a toujours l'obstacle de l'eclair
    fenceAvailable[2] = obstaclesType[2];
    // obstacles de la prairie
    if (backgroundUsed == 1  ) {
      fenceAvailable[0] = obstaclesType[0];
      fenceAvailable[1] = obstaclesType[1];
    }
    // obstacle neige
    if ( backgroundUsed == 2 ) {
      fenceAvailable[0] = obstaclesType[5];
      fenceAvailable[1] = obstaclesType[6];
    }
    // obstacle du desert
    if ( backgroundUsed == 3 ) {
      fenceAvailable[0] = obstaclesType[3];
      fenceAvailable[1] = obstaclesType[4];
    }
    if ( backgroundUsed == 4 ) {
      fenceAvailable[0] = obstaclesType[7];
      fenceAvailable[1] = obstaclesType[8];
    }

    var obstacle  = fenceAvailable[parseInt(random(0,3))];
    //var obstacle = obstaclesType[parseInt(random(0, obstaclesType.length))];
    obstacles.push(new Obstacle(sheep.getPosition().x + random(500, 900), obstacle));
  }

  // Comptage des obstacles passés et collide
  obstaclesPassed = 0;
  for (var i = 0; i < obstacles.length; i++) {
    // Si le mouton a passer l'obstacle
    if (obstacles[i].isPassed(sheep.spr.position.x)) obstaclesPassed++;
    obstacles[i].draw();

    // Si le mouton touche un obstacle
    if (sheep.spr.overlap(obstacles[i].sprite()) && !sheep.boosted) {
      sheep.alive = false;
      sheep.spr.changeAnimation("dying");
      sheep.spr.animation.looping = false;
    }
  }
  textSize(40)
  text(obstaclesPassed, sheep.getPosition().x + 420, 40)




  camera.position.x = sheep.getPosition().x;

  drawSprites();

  if (!sheep.alive) {
    endGame();
  }
  textSize(40)
  text(obstaclesPassed, sheep.getPosition().x + 420, 40)
}

function keyPressed() {
  if (keyCode == 32 && !moise_character) {
    if (start  && sheep.alive) {
      sheepJumping = true;
      sheep.spr.changeAnimation("jump_up");
      sheep.spr.velocity.y -= 20;

      sprSmoke = createSprite(sheep.spr.position.x, sheep.spr.position.y, 5, 5);
      sprSmoke.addAnimation("smoke",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_1.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_2.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_3.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_4.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_5.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_6.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_7.png",
        DIR_ASSETS+"sheep_animation/jump/smoke/smoke_8.png");
      sprSmoke.animation.looping = false;
    }
    else if(!sheep.alive) {
      endGameDone = false;
      for (var i = 0; i < obstacles.length; i++) {
        obstacles[i].sprite().remove();
      }
      obstacles = [];
      for (var i = 0; i < kebabs.length; i++) {
        kebabs[i].sprite().remove();
      }
      kebabs = [];
      sheep.alive = true;
      sheep.spr.position.x = 250;
      sheep.spr.position.y = 200;
      sheep.spr.velocity.x = 6;
    }
    else{
      endGameDone = false;
      start = true;
      loop();
      sheep.spr.velocity.x = 6;
    }
  }
  //Apparition de Moise lorsque le mouton et mort et qu'on appuie sur la touche "m"
  if(keyCode == 77 && !sheep.alive && !moise_character) {
    moise_character = new Moise(sheep.spr.position.x + 200, 300, sheep);
    console.log(sheep.spr.position.x);
  }
}

function endGame() {
  if(endGameDone == false){
    sendScore(obstaclesPassed);
    console.log("coucou");
    endGameDone = true;
  }
  if (localStorage.getItem('score') < obstaclesPassed) {
      localStorage.setItem('score', obstaclesPassed)
    }
    textSize(40);
    text("You lose! Press SPACE to restart", sheep.spr.position.x - 300, height / 2);
    text("Your best score : " + localStorage.getItem("score"), sheep.spr.position.x - 500, 40);

}

function sendScore(obstaclesPassed){
  var sec = obstaclesPassed;
  xhr = new XMLHttpRequest();
  xhr.open('POST', 'php/sendscore.php?name='+nickname);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
      if (xhr.status === 200 && xhr.responseText !== "OK") {
          console.log('Something went wrong.  Name is now ' + xhr.responseText);
      }
      else if (xhr.status !== 200) {
          console.log('Request failed.  Returned status of ' + xhr.status);
      }
      getScore();
  };
  xhr.send(encodeURI('score='+obstaclesPassed+'&time='+sec+'&s='+sec+'&c='+sec+'&o='+sec+'&r='+sec+'&e='+sec+'&l='+sec+'&hackme=false&k=caca'));
}

function getScore(){
  xhr = new XMLHttpRequest();
  xhr.open('POST', 'php/getscore.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
      if (xhr.status === 200 && xhr.responseText !== "OK") {
          var data = JSON.parse(xhr.responseText);
          html = "<table>";
          html += data.map((e,key) => {
            return "<tr><td>"+(key+1)+"</td><td>"+e.name+"</td><td>"+e.score+" points</td></tr>";
          });
          html+="</table>";
          document.getElementById('scoreboard').innerHTML = html;
      }
      else if (xhr.status !== 200) {
          console.log('Request failed.  Returned status of ' + xhr.status);
      }
  };
  xhr.send(encodeURI('&hackme=false'));
}
