class Obstacle {
  constructor(x, obstacle) {
    this.x = x;
    this.spr = createSprite(0, obstacle.y, 5, 5);
    this.spr.addImage(obstacle.image);
    this.spr.scale = obstacle.scale;
    this.spr.setCollider("rectangle", obstacle.collider.offsetX, obstacle.collider.offsetY, obstacle.collider.width, obstacle.collider.height);
  }
  update() {
  }
  draw() {
    this.update();
    this.spr.position.x = this.x;
  }
  sprite() {
    return this.spr
  }
  isPassed(x) {
    return x > this.spr.position.x ? true : false;
  }
}
