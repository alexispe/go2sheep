class Moise {
  constructor(x, y, sheep) {
    this.player = sheep;
    this.x = x;
    this.y = y;

    this.sprMoise = createSprite(x, y, 50, 90);
    this.sprMoise.addAnimation("coming",
        DIR_ASSETS+"bonus/moise/moise_1.png",
        DIR_ASSETS+"bonus/moise/moise_2.png",
        DIR_ASSETS+"bonus/moise/moise_3.png",
        DIR_ASSETS+"bonus/moise/moise_4.png",
        DIR_ASSETS+"bonus/moise/moise_5.png");
    this.sprMoise.animation.looping = false;

    this.sprResurection = createSprite(sheep.spr.position.x, y - 50, 50, 90);
    this.sprResurection.addAnimation("coming",
        DIR_ASSETS+"bonus/resurection/resurection_01.png",
        DIR_ASSETS+"bonus/resurection/resurection_02.png",
        DIR_ASSETS+"bonus/resurection/resurection_03.png",
        DIR_ASSETS+"bonus/resurection/resurection_04.png",
        DIR_ASSETS+"bonus/resurection/resurection_05.png");
    this.sprResurection.animation.looping = false;
    this.sheepResurection(sheep);
  }
  update() {

  }
  draw() {
    this.update();
  }
  sprite() {
    return this.sprMoise;
  }
  isPassed(x) {
    return (x-500) > this.sprMoise.position.x ? true : false;
  }
  sheepResurection(theSheep) {
      theSheep.resurect = true;
      theSheep.spr.velocity.y = -1;
  }
}



