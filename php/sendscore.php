<?php
$number_jumps = 0;
$name = htmlspecialchars($_GET['name']);


function get_ip() {
	// IP si internet partagé
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		return $_SERVER['HTTP_CLIENT_IP'];
	}
	// IP derrière un proxy
	elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	// Sinon : IP normale
	else {
		return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
	}
}


if(isset($_POST['o']) && isset($_POST['k'])) {
	$score = $_POST['o'];
	$date = date('Y-m-d H:i');
	$ip = get_ip();
	// Create connection
	$pdo=new PDO("mysql:host=localhost;dbname=mouton;","mouton","moutmout");
	//$pdo = new PDO('sqlite:db.sqlite');

	$stmt = $pdo->prepare("INSERT INTO score (name, score, number_jumps, date, ip) VALUES (:name, :score, :number_jumps, :date, :ip)");
	$stmt->bindParam(':name', $name);
	$stmt->bindParam(':number_jumps', $number_jumps);
	$stmt->bindParam(':score', $score);
	$stmt->bindParam(':date', $date);
	$stmt->bindParam(':ip', $ip);
	$stmt->execute();

	echo 'OK';

}
else echo 'SQLERROR : Parameter "score" is missing';

if(isset($_POST['hackme']) && $_POST['hackme']=='true') echo 'ptdr t ki ?';

?>
