class Kebab {
  constructor(x, y, img) {
    this.x = x;
    this.y = y;

    this.spr = createSprite(x, y, 50, 90);
    this.spr.addImage(img);
  }
  update() {
  }
  draw() {
    this.update();
    this.spr.position.x = this.x;
    this.spr.position.y = this.y;
  }
  sprite() {
    return this.spr
  }
  isPassed(x) {
    return x > this.spr.position.x ? true : false;
  }
}
