class Sheep {
  constructor() {
    this.spr = createSprite(0, FLOOR_POS, 5, 5);
    this.spr.setCollider("rectangle", 10, 10, 250, 200);
    this.spr.velocity.x = DEFAULT_SPEED;
    this.spr.velocity.y = 0;
    this.spr.scale = 0.3;

    this.loadAnimations();
    this.spr.changeAnimation("walking");

    this.jumping = false;
    this.boosted = false;
    this.alive = true;
    this.resurect = false;
  }
  loadAnimations() {
    this.spr.addAnimation("walking",
      DIR_ASSETS+"sheep_animation/walk/walk_0.png",
      DIR_ASSETS+"sheep_animation/walk/walk_1.png",
      DIR_ASSETS+"sheep_animation/walk/walk_2.png",
      DIR_ASSETS+"sheep_animation/walk/walk_3.png",
      DIR_ASSETS+"sheep_animation/walk/walk_4.png",
      DIR_ASSETS+"sheep_animation/walk/walk_5.png",
      DIR_ASSETS+"sheep_animation/walk/walk_6.png",
      DIR_ASSETS+"sheep_animation/walk/walk_7.png",
      DIR_ASSETS+"sheep_animation/walk/walk_8.png",
      DIR_ASSETS+"sheep_animation/walk/walk_9.png",
      DIR_ASSETS+"sheep_animation/walk/walk_10.png");
    this.spr.addAnimation("jump_up",
      DIR_ASSETS+"sheep_animation/jump/jump_0.png",
      DIR_ASSETS+"sheep_animation/jump/jump_1.png",
      DIR_ASSETS+"sheep_animation/jump/jump_2.png");
    this.spr.addAnimation("jump_down",
      DIR_ASSETS+"sheep_animation/jump/jump_3.png",
      DIR_ASSETS+"sheep_animation/jump/jump_4.png");
    this.spr.addAnimation("dying",
      DIR_ASSETS+"sheep_animation/die/die_0.png",
      DIR_ASSETS+"sheep_animation/die/die_1.png",
      DIR_ASSETS+"sheep_animation/die/die_2.png",
      DIR_ASSETS+"sheep_animation/die/die_3.png",
      DIR_ASSETS+"sheep_animation/die/die_4.png");
    this.spr.addAnimation("die_fall",
      DIR_ASSETS+"sheep_animation/die/die_bloody.png");

  }
  update(start) {
     // if(start)
     if(!this.resurect || start) {
      this.spr.velocity.y += 1.5;
    }
    else {
      if (this.spr.position.y < 0) {
        this.resurect = false;
        this.alive = true;
        sheep.spr.velocity.x = BOOST_SPEED;
        sheep.boosted = true;
        setTimeout(function(){
        sheep.spr.velocity.x = DEFAULT_SPEED;
      }, BOOST_DURATION);
      setTimeout(function(){
        sheep.boosted = false;
      }, BOOST_DURATION_INVINCIBILITY);
      }
    }

    // Si le mouton tombe sous la ligne du sol
    if (this.spr.position.y > 300) {
      this.jumping = false;
      this.spr.position.y = 300;
      this.spr.velocity.y = 0;
    }
    if(this.alive) {

      this.spr.velocity.x *= VELOCITY_SPEED;
      this.spr.changeAnimation("walking");

      // Si le mouton est au dessus du plafond
      if (this.spr.position.y < 0) {
        this.spr.position.y = 0;
      }
      // Si le mouton tombe (se dirige vers le bas)
      if (this.spr.velocity.y == 1) {
        this.spr.changeAnimation("jump_down");
      }
      // Si le mouton est rapide et qu'il est sous la ligne des 200
      if (this.spr.getSpeed() > 30 && this.spr.position.y > 200 && this.spr.velocity.y > 0 && !this.boosted) {
        // Il meurt
        this.alive = false;
        this.spr.changeAnimation("die_fall");
      }
    }
    else {
      this.spr.velocity.x = 0;
    }
  }
  run(start) {
    this.update(start)
  }
  getPosition() {
    return {'x':this.spr.position.x,'y':this.spr.position.y};
  }
}
