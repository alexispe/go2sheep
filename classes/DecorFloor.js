class DecorFloor {
  constructor(x, y, w, h, img, type) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.type = type;
    this.spr = createSprite(0, FLOOR_POS, 0,0);
    this.spr.addImage(img);
    this.spr.position.x = this.x;
    this.spr.velocity.x = (type=='cloud') ? random(5,20) : 0;
  }
  update() {
  }
  draw() {
    this.update();
    //this.spr.position.x = this.x;
    this.spr.position.y = this.y;
  }
  sprite() {
    return this.spr
  }
  canBeDeleted(x) {
    return (x-600) > this.spr.position.x ? true : false;
  }

}
